'use strict';
const fs = require('fs');

class Logger {

  constructor(config) {
    if (!Logger.instance) {
      Logger.instance = this;
    } else {
      return Logger.instance;
    }

    if (!config || !config.path || !config.levels) {
      throw new TypeError('Invalid arguments, missing config.filename or config.logpath or config.levels');
    }

    this._path = config.path;
    this._levels = config.levels;

    this._ws = fs.createWriteStream(this._path, {
      flags: 'a'
    });
    this.start();
    return Logger.instance;

  }

  trace() {
    this.stackTrace();
  }

  stackTrace() {
    var err = new Error();
    this._ws.write(JSON.stringify({
      date: (new Date).toISOString(),
      type: 'trace',
      msg: err.stack.replace(/\s+/g, ' ')
    }, null, 2) + '\n');
  }

  _write(type = 'info', msg) {
    this._ws.write(JSON.stringify({
      date: (new Date).toISOString(),
      type: type,
      msg: msg
    }) + '\n');
  }

  start() {
    this._write('start', '=== start ===');
  }
  end() {
    this._write('end', '=== end ===');
    this._ws.end();
  }

  debug(...msg) {
    this._write('debug', msg);
  }

  info(...msg) {
    this._write('info', msg);
  }

  notice(...msg) {
    this._write('notice', msg);
  }


  warn(...msg) {
    this._write('warn', msg);
    this.stackTrace();
  }

  error(...msg) {
    this._write('error', msg);
    this._ws.write(this.stackTrace() + '\n');
  }

  getInstance(config) {
    new Logger(config);
  }

}


Object.freeze(Logger.instance);

module.exports = Logger;
